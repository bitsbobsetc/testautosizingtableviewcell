//
//  TestCell.swift
//  TestTableViewApp
//
//  Created by BitsBobsEtc on 06/05/2019.
//  Copyright © 2019 BitsBobsEtc. All rights reserved.
//

import UIKit

class TestCell: UITableViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0))
    }
}
