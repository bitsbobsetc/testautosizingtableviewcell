//
//  ViewController.swift
//  TestTableViewApp
//
//  Created by BitsBobsEtc on 06/05/2019.
//  Copyright © 2019 BitsBobsEtc. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController, UITableViewDelegate {

    @IBOutlet weak var testTableView: UITableView!
    
    private let disposeBag = DisposeBag()
    
    let items = BehaviorRelay<[String]>(value: [])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.testTableView.delegate = self
        
        //Register cell
        self.testTableView.register(UINib(nibName: "TestCell", bundle: nil), forCellReuseIdentifier: "testCell")
        
        //Generate cells
        items.asObservable()
            .bind(to: testTableView.rx.items(cellIdentifier: "testCell", cellType: TestCell.self)) { (_, element, cell) in
                
                cell.cellImage.image = UIImage(named: "samplePic")
                
            }
            .disposed(by: self.disposeBag)
        
        //Create cells
        items.accept(["", "", "", "", "", "", "", "", "", ""])
    }
}

